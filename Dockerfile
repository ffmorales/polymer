# Imagen base
FROM node:latest

# Imagen base
WORKDIR /app

# Copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Pruerto que expongo
EXPOSE 3000

#Comando para ejecutar el servicio
CMD ["npm","start"]
